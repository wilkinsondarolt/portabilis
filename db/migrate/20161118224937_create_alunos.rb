class CreateAlunos < ActiveRecord::Migration[5.0]
  def change
    create_table :alunos do |t|
      t.string :cpf, limit:11
      t.string :rg, limit:20
      t.date :data_nascimento
      t.string :nome
      t.string :telefone
      t.timestamps
    end
  end
end
