class AddColumnAlunoIdAndCursoIdToMatricula < ActiveRecord::Migration[5.0]
  def change
    add_column :matriculas, :aluno_id, :integer
    add_column :matriculas, :curso_id, :integer
  end
end
