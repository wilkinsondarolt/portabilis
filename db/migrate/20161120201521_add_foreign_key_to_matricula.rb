class AddForeignKeyToMatricula < ActiveRecord::Migration[5.0]
  def change
    add_foreign_key :matriculas, :cursos
    add_foreign_key :matriculas, :alunos
  end
end
