class CreateMatriculas < ActiveRecord::Migration[5.0]
  def change
    create_table :matriculas do |t|
      t.date :data_matricula
      t.integer :ano
      t.string :ativo, limit:1
      t.string :pago, limit:1

      t.timestamps
    end
  end
end
