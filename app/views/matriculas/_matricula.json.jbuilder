json.extract! matricula, :id, :data_matricula, :ano, :ativo, :pago, :created_at, :updated_at
json.url matricula_url(matricula, format: :json)