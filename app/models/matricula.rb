class Matricula < ApplicationRecord
    belongs_to :aluno
    belongs_to :curso

    scope :ativos, -> {where(ativo: 'S')}

    validates_presence_of(:aluno_id, message: " - deve ser preenchido")
    validates_associated :aluno
    validates_presence_of(:curso_id, message: " - deve ser preenchido")
    validates_associated :curso
    validates_presence_of(:data_matricula, message: " - deve ser preenchido")
    validates_presence_of(:ano, message: " - deve ser preenchido")
    validates_numericality_of(:ano, greater_than: 0, message: " - deve ser um valor maior que zero")
    validates_numericality_of(:ano, greater_than_or_equal_to: Date.today.year, message: " - deve ser maior ou igual ao ano atual (#{Date.today.year})")
    validate :validar_unicidade_matricula

    private
    def validar_unicidade_matricula
      if ano != nil
        matriculas = Matricula.ativos.where("aluno_id = #{aluno_id} and ano = #{ano}")

        matriculas.each do |matricula|
          if curso.periodo==3||[curso.periodo, 3].include?(matricula.curso.periodo)
            errors.add(:curso_id, " - já existe uma matrícula para o curso de '#{matricula.curso.nome}' no mesmo período.")
            break
          end
        end
      end
    end
end
