class Aluno < ApplicationRecord
  has_many :matriculas
  validates_presence_of(:nome, message: " - deve ser preenchido")
  validates_presence_of(:cpf, message: " - deve ser preenchido")
  validates_length_of(:cpf, maximum: 11, message: " - deve conter 11 caracteres")
  validates_uniqueness_of(:cpf, message: " - já está cadastrado.")
  validates_numericality_of(:cpf, message: " - deve conter somente números")
  validate :validar_cpf

  validates_presence_of(:rg, message: " - deve ser preenchido")
  validates_presence_of(:data_nascimento, message: " - deve ser preenchido")
  
  private
  def validar_cpf
    nulos = %w{12345678909 11111111111 22222222222 33333333333 44444444444 55555555555 66666666666 77777777777 88888888888 99999999999 00000000000}
    errors.add(:cpf, " - deve ser válido ") if !Cpf.new(cpf).valido?||nulos.member?(cpf)
  end
end
