class Caixa
  attr_accessor :valordivida
  attr_accessor :valorpago
  attr_accessor :valortroco

  def initialize
    @valordivida = 0.00
    @valorpago = 0.00
    @valortroco = 0.00
  end

  def calcula_valor_troco
    if @valorpago > @valordivida
      return (@valorpago - @valordivida).round(2)
    end
    return 0.00
  end

  def DefinirMelhorTroco()
    unidades = [{tipo:'cédula', nome:'100', valor:100},
                {tipo:'cédula', nome:'50', valor:50},
                {tipo:'cédula', nome:'10', valor:10},
                {tipo:'cédula', nome:'5', valor:5},
                {tipo:'cédula', nome:'1', valor:1},
                {tipo:'moeda', nome:'50', valor:0.50},
                {tipo:'moeda', nome:'10', valor:0.10},
                {tipo:'moeda', nome:'5', valor:0.05},
                {tipo:'moeda', nome:'1', valor:0.01}]
    saldo = 0.00
    @valortroco = calcula_valor_troco
    saldo = @valortroco

    if saldo > 0
      unidades.each do |unidade|
        qtde = (saldo / unidade[:valor]).to_i
        saldo = (saldo - (qtde * unidade[:valor])).round(2)
        if qtde > 0
          if block_given?
            yield qtde, unidade[:tipo], unidade[:nome]
          end
        end
        if saldo == 0
          break
        end
      end
    end
  end
end
