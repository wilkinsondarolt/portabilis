class Curso < ApplicationRecord
  has_many :matriculas
  validates_presence_of(:nome, message: " - deve ser preenchido")
  validates_presence_of(:valor_inscricao, message: " - deve ser preenchido")
  validates_numericality_of(:valor_inscricao, greater_than: 0, message: " - deve ser um valor maior que zero")
  validates_presence_of(:periodo, message: " - deve ser preenchido")
  validates_inclusion_of :periodo, :in => [1, 2, 3], message: " - deve ser 'Vespertino', 'Matutino' ou 'Integral'"

  def periodo_texto
    case periodo
    when 1 then
      return 'Vespertino'
    when 2 then
      return 'Matutino'
    when 3 then
      return 'Integral'
    end
  end
end
