class MatriculasController < ApplicationController
  before_action :set_matricula, only: [:show, :edit, :update, :destroy, :inativar, :pagar]

  def preparar_form
    @alunos = Aluno.order :nome
    @cursos = Curso.order :nome
  end
  # GET /matriculas
  # GET /matriculas.json
  def index
    @matriculas = Matricula.ativos
    @matriculas.includes(:aluno, :curso)
  end

  # GET /matriculas/1
  # GET /matriculas/1.json
  def show
  end

  # GET /matriculas/new
  def new
    @matricula = Matricula.new
    @matricula.ativo ||= 'S'
    @matricula.pago ||= 'N'
    @matricula.ano ||= Date.today.year
    preparar_form
  end

  # GET /matriculas/1/edit
  def edit
    preparar_form
  end

  # POST /matriculas
  # POST /matriculas.json
  def create
    @matricula = Matricula.new(matricula_params)

    respond_to do |format|
      if @matricula.save
        format.html { redirect_to @matricula, notice: 'Matrícula cadastrada com sucesso.' }
        format.json { render :show, status: :created, location: @matricula }
      else
        preparar_form
        format.html { render :new }
        format.json { render json: @matricula.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /matriculas/1
  # PATCH/PUT /matriculas/1.json
  def update
    if params[:matricula] && params[:matricula][:valor_pago]
      pagamento(@matricula.id, params[:matricula][:valor_pago])
    else
      respond_to do |format|
        if @matricula.update(matricula_params)
          format.html { redirect_to @matricula, notice: 'Matrícula atualizada com sucesso.' }
          format.json { render :show, status: :ok, location: @matricula }
        else
          preparar_form
          format.html { render :edit }
          format.json { render json: @matricula.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /matriculas/1
  # DELETE /matriculas/1.json
  def destroy
    @matricula.destroy
    respond_to do |format|
      format.html { redirect_to matriculas_url, notice: 'Matrícula removida com sucesso.' }
      format.json { head :no_content }
    end
  end

  def inativar()
    @matricula.update_attribute(:ativo, 'N')
    @matricula.save
    respond_to do |format|
      format.html { redirect_to matriculas_url, notice: 'Matrícula inativada com sucesso.' }
      format.json { head :no_content }
    end
  end
  helper_method :inativar

  def pagar
    if @matricula.pago == "S"
      redirect_to matriculas_url, notice: 'Valor de inscrição já foi pago.'
    end
  end
  helper_method :pagar

  def pagamento(id, valorpago)
    matricula = Matricula.find(id)
    matricula.update_attribute(:pago, 'S')
    matricula.save
    caixa = Caixa.new
    caixa.valordivida = @matricula.curso.valor_inscricao
    caixa.valorpago = valorpago.to_f

    troco = []
    troco << "Troco: R$ #{caixa.calcula_valor_troco}"
    caixa.DefinirMelhorTroco do |qtde, tipo, nome|
      troco << "#{qtde}x #{tipo}(s) de #{nome} \n"
    end
    redirect_to matriculas_url, notice: "Pagamento efetuado com sucesso!" + troco.join("\n").html_safe
  end
  helper_method :pagamento

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_matricula
      @matricula = Matricula.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def matricula_params
      params.require(:matricula).permit(:data_matricula, :ano, :ativo, :pago, :aluno_id, :curso_id, :valor_pago)
    end
end
