Rails.application.routes.draw do
  root 'matriculas#index'
  resources :alunos
  resources :matriculas
  match 'matriculas/:id/pagar' => 'matriculas#pagar', controller: 'matriculas', action: 'pagar', via: :get
  match 'matriculas/:id' => 'matriculas#inativar', controller: 'matriculas', action: 'inativar', via: :post
  resources :cursos
  resources :qualificacaos

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
